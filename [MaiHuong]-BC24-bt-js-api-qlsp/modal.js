class product {
  constructor(id,name,img,type,desc,screen,frontCamera,backCamera,price) {
    this.id = id;
    this.name = name;
    this.img = img;
    this.type = type;
    this.desc = desc;
    this.screen=screen;
    this.frontCamera=frontCamera;
    this.backCamera=backCamera;
    this.price = price;
  }
}
