const BASE_URL = "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products";

let showList = () => {
  axios({
    url: `${BASE_URL}/`,
    method: "GET",
  })
    .then(function (res) {
      console.log("yes", res.data);
      renderTable(res.data);
    })
    .catch(function (err) {
      console.log("no", err);
    });
};
showList();
// show product list
let renderTable = (arr) => {
  var contentHTML = "";
  for (var i = 0; i < arr.length; i++) {
    var product = arr[i];
    var divContent = `
    <tr>
    <td>${product.id}</td>
    <td>${product.name}</td>
    <td><img class="img-fluid" src="${product.img}"/></td>
    <td>${product.type}</td>
    <td>${product.screen}</td>
    <td>${product.backCamera}</td>
    <td>${product.frontCamera}</td>    
    <td>${product.desc}</td>
    <td>${product.price}</td>    
<td>
    <button class="btn btn-success" onclick=updateProduct(${product.id})>Sửa</button>
    <button class="btn btn-danger" onclick=deleteProduct(${product.id})>Xóa</button>
    </td>    
    </tr>
      `;
    contentHTML += divContent;
  }
  document.getElementById("productList").innerHTML = contentHTML;
};
// edit product
let updateProduct = (id) => {
  axios({
    url: `${BASE_URL}/${id}`,
    method: "GET",
  })
    .then(function (res) {
      console.log(res.data);
      document.getElementById("product_id").value = res.data.id;
      document.getElementById("product_name").value = res.data.name;
      document.getElementById("product_img").value = res.data.img;
      document.getElementById("product_type").value = res.data.type;
      document.getElementById("product_description").value = res.data.desc;
      document.getElementById("product_price").value = res.data.price;
      document.getElementById("product_screen").value = res.data.screen;
      document.getElementById("product_frontCamera").value =
        res.data.frontCamera;
      document.getElementById("product_backCamera").value = res.data.backCamera;
    })
    .catch(function (err) {
      console.log("err", err);
    });
};
document.getElementById("updateButton").addEventListener("click", function () {
  let productId = document.getElementById("product_id").value;
  let productName = document.getElementById("product_name").value;
  let productImg = document.getElementById("product_img").value;
  let productType = document.getElementById("product_type").value;
  let productDesc = document.getElementById("product_description").value;
  let productPrice = document.getElementById("product_price").value;
  let productScreen = document.getElementById("product_screen").value;
  let productFrontCam = document.getElementById("product_frontCamera").value;
  let productBackCam = document.getElementById("product_backCamera").value;
  let updateProduct = new product(
    productId,
    productName,
    productImg,
    productType,
    productDesc,
    productScreen,
    productFrontCam,
    productBackCam,
    productPrice
  );
  axios({
    url: `${BASE_URL}/${productId}`,
    method: "PUT",
    data: updateProduct,
  })
    .then(function (res) {
      showList();
      alert("Cập nhật thành công");
      document.getElementById("resetButton").click();
    })
    .catch(function (err) {
      console.log("err", err);
    });
});
// add product
document.getElementById("addButton").addEventListener("click", function () {
  let productId = document.getElementById("product_id").value;
  let productName = document.getElementById("product_name").value;
  let productImg = document.getElementById("product_img").value;
  let productType = document.getElementById("product_type").value;
  let productDesc = document.getElementById("product_description").value;
  let productPrice = document.getElementById("product_price").value;
  let productScreen = document.getElementById("product_screen").value;
  let productFrontCam = document.getElementById("product_frontCamera").value;
  let productBackCam = document.getElementById("product_backCamera").value;
  let addProduct = new product(
    productId,
    productName,
    productImg,
    productType,
    productDesc,
    productScreen,
    productFrontCam,
    productBackCam,
    productPrice
  );
  axios({
    url: `${BASE_URL}/`,
    method: "POST",
    data: addProduct,
  })
    .then(function (res) {
      showList();
      alert("Cập nhật thành công");
      document.getElementById("resetButton").click();
    })
    .catch(function (err) {
      console.log("err", err);
    });
});
// delete product
let deleteProduct = (id) => {
  axios({
    url: `${BASE_URL}/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      console.log(res.data);
      alert("Xóa sản phẩm thành công");
      showList();
    })
    .catch(function (err) {
      console.log("err", err);
    });
};
