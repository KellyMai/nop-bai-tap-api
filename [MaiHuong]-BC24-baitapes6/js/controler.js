// lấy data
let contentHTML = "";

let getInfo = () => {
  let ho = document.querySelector('[name="ho"]');
  let ten = document.querySelector('[name="ten"]');
  let email = document.querySelector('[name="email"]');
  let phone = document.querySelector('[name="soDienThoai"]');
  let ngay = document.querySelector('[name="ngay"]');
  let thang = document.querySelector('[name="thang"]');
  let nam = document.querySelector('[name="nam"]');
  let address = document.querySelector('[name="diaChi"]');
  let transport = document.querySelector('[name="thongTinDiChuyen"]');
  let typeF = document.querySelector('[name="loaiNhiem"]');
  // tạo biến flag kiểm tra ô đã nhập hay chưa
  let flagTen = false;
  let flagHo = false;
  let flagEmail = false;
  let flagPhone = false;
  let flagDob = false;
  let flagMob = false;
  let flagYob = false;
  let flagAddress = false;
  let flagTransport = false;
  let flagTypeF = false;

  ten.addEventListener("change", function () {
    contentHTML += `<p>Tên: ${ten.value}</p>`;
    flagTen = true;
    showInfo();
  });
  ho.addEventListener("change", function () {
    contentHTML += `<p>Họ:${ho.value}</p>`;
    flagHo = true;
    showInfo();
  });

  email.addEventListener("change", function () {
    contentHTML += `<p>Email: ${email.value}</p>`;
    flagEmail = true;
    showInfo();
  });

  phone.addEventListener("change", function () {
    contentHTML += `<p>Số điện thoại: ${phone.value}</p>`;
    flagPhone = true;
    showInfo();
  });

  ngay.addEventListener("change", function () {
    contentHTML += `<p>Ngày tháng năm sinh: ${ngay.value}`;
    flagDob = true;
    showInfo();
  });

  thang.addEventListener("change", function () {
    contentHTML += `/ ${thang.value}`;
    flagMob = true;
    showInfo();
  });

  nam.addEventListener("change", function () {
    contentHTML += `/ ${nam.value} </p>`;
    flagYob = true;
    showInfo();
  });
  // save data
  address.addEventListener("change", function () {
    contentHTML += `<p> Địa chỉ: ${address.value}</p>`;
    flagAddress = true;
    showInfo();
  });

  transport.addEventListener("change", function () {
    contentHTML += `<p> Thông tin di chuyển: ${transport.value} </p>`;
    flagTransport = true;
    showInfo();
  });

  typeF.addEventListener("change", function () {
    contentHTML += `<p> Bạn nghĩ bạn là F mấy? ${typeF.value}</p>`;
    flagTypeF = true;
    showInfo();
  });

  let showInfo = () => {
    // show info
    document.querySelector("div.donate-us").innerHTML = contentHTML;
    let flag =
      flagTen &&
      flagHo &&
      flagEmail &&
      flagPhone &&
      flagDob &&
      flagMob &&
      flagYob &&
      flagAddress &&
      flagTransport &&
      flagTypeF;    
    if (flag) {
      let newUser = new userKBYT(
        ho.value,
        ten.value,
        email.value,
        phone.value,
        ngay.value,
        thang.value,
        nam.value,
        address.value,
        transport.value,
        typeF.value
      );
      console.log(newUser);
    }
  };
};
